FROM python:3.10.0-buster
COPY . /myapp
WORKDIR /myapp
RUN pip install -r requirements.txt
CMD [ "python", "application.py" ]